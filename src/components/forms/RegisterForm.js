import React, { useState } from "react";

const RegisterForm = props => {

const [username, setUsername] = useState("");
const [password, setPassword] = useState("");

    const onRegisterClicked = event => {

        console.log("EVENT:", event.target);

        props.click({
            success: true,
            message: "registered successfully"
        });

    };

    const onUsernameChanged = event => 

    return (
        <form>
            <div>
                <label>Username: </label>
                <input type="text" placeholder="Enter a username..."></input>
            </div>
            <div>
                <label>Password: </label>
                <input type="password" placeholder="Enter a password..."></input>
            </div>
            <div>
                <button type="button" onClick={onRegisterClicked} >Register</button>
            </div>


        </form>
    )

};

export default RegisterForm;